"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import sys
import json

class Parameters:

    def __init__(self):

        self.clear()
        
    def clear(self):

        self.parameters = {'parameters' : []}
        self.returnParameters = {'parameters' : []}
        
    def getParameters(self):

        return json.dumps(self.parameters)
        
    def setParameters(self, string):

        self.returnParameters = json.loads(string)
        
    def registerIntParameter(self, name, description="", defaultValue=0, min=-2**31, max=2**31-1):

        var = {
                'name' : name,
                'type' : 'int',
                'description' : description,
                'defaultValue' : str(defaultValue),
                'intmin' : min,
                'intmax' : max,
            }
        
        self.parameters['parameters'].append(var)
        
    def registerFloatParameter(self, name, description="", defaultValue=0, decimals=2 , min=-sys.float_info.max, max=sys.float_info.max):

        var = {
                'name' : name,
                'type' : 'float',
                'description' : description,
                'defaultValue' : str(defaultValue),
                'decimals' : decimals,
                'floatmin' : min,
                'floatmax' : max,
            }
        
        self.parameters['parameters'].append(var)
        
    def registerBoolParameter(self, name, description="", defaultValue=True):

        var = {
                'name' : name,
                'type' : 'bool',
                'description' : description,
                'defaultValue' : 'true' if defaultValue else 'false',
            }
        
        self.parameters['parameters'].append(var)
        
    def registerStringParameter(self, name, description="", defaultValue=""):

        var = {
                'name' : name,
                'type' : 'string',
                'description' : description,
                'defaultValue' : defaultValue,
            }
        
        self.parameters['parameters'].append(var)
        
    def registerJsonParameter(self, name, description="", defaultValue={}):

        var = {
                'name' : name,
                'type' : 'json',
                'description' : description,
                'defaultValue' : json.dumps(defaultValue),
            }
        
        self.parameters['parameters'].append(var)
        
    def getParameter(self, name):

        for var in self.returnParameters['parameters']:

            if var['name'] == name:

                return var['value']
        
    def getIntParameter(self, name):

        return int(self.getParameter(name))
        
    def getFloatParameter(self, name):

        return float(self.getParameter(name))
        
    def getBoolParameter(self, name):

        return self.getParameter(name) == "true"
        
    def getStringParameter(self, name):

        return self.getParameter(name)
        
    def getJsonParameter(self, name):

        return json.loads(self.getParameter(name))


