"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import time
import os

from mseg import DataExchangeInterfacePrx

class DataExchangeProxy:

    def __init__(self):

        self.data = None
        
    def connect(self, ic):

        de = ic.stringToProxy('DataExchange')
        self.data = DataExchangeInterfacePrx.checkedCast(de)
        
    def getFrameCount(self):

        return self.data.getFrameCount()
        
    def getMMMFile(self):

        return self.data.getMMMFile()

    def getViconFile(self):

        return self.data.getViconFile()
        
    def getGroundTruth(self):

        return self.data.getGroundTruth()
        
    def getJointAnglesForFrame(self, frame):

        return self.data.getJointAnglesForFrame(frame)

    def reportKeyFrame(self, frame):

        self.data.reportKeyFrame(frame)

    def reportKeyFrames(self, frames):

        self.data.reportKeyFrames(frames)
