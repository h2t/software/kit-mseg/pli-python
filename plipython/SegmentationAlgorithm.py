"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import sys
from abc import ABCMeta, abstractmethod

import json

from mseg import SegmentationAlgorithmInterface
from mseg import Granularity

from Parameters import Parameters
from DataExchangeProxy import DataExchangeProxy

class SegmentationAlgorithm(SegmentationAlgorithmInterface, Parameters):
    __metaclass__ = ABCMeta
    
    def  __init__(self):

        Parameters.__init__(self)

        self.data = DataExchangeProxy()
        self.name = ""
        self.requiresTraining = False
        self.trainingGranularity = Granularity.Medium

    @abstractmethod
    def segment():

        pass
        
    def train(self):

        pass
        
    def resetTraining(self):

        pass
        
    def setRequireTraining(self, requiresTraining):

        self.requiresTraining = requiresTraining
        
    def internal_requiresTraining(self, current=None):

        return self.requiresTraining

    def internal_getTrainingGranularity(self, current=None):

        return self.trainingGranularity
        
    def internal_train(self, current=None):

        self.train()
        
    def internal_resetTraining(self, current=None):

        self.resetTraining()
        
    def internal_segment(self, current=None):

        self.segment()

    def internal_getParameters(self, current=None):

        return self.getParameters()
        
    def internal_setParameters(self, string, current=None):

        self.setParameters(string)
        
    def internal_getName(self, current=None):

        return self.name
